You are subscribed to receive update notifications for incident #{{incident.pk}}.

The investigation for this incident is now complete.  Incident & investigation details are given below.

=== Details for Incident #{{incident.pk}} ===

Date Submitted      : {{incident.submitted}}
Incident Date       : {{incident.incident_date}}
PSLS ID             : {% firstof investigation.psls_id "N/A" %}
Actual/Potential    : {{incident.get_actual_display}}{% if incident.needs_standard_description%}
Std. Description    : {{incident.standard_description}}{% endif %}
Location            : {{incident.location}}
Severity            : {% firstof incident.get_severity_display "" %}
Treatment Intent    : {{incident.get_intent_display}}
Treatment Technique : {{incident.technique}}
# Patients Affected : {{incident.get_num_affected_display}}

=== Incident Description  ===

{{incident.description}}

=== Investigation Details ===

Investigator        : {{investigation.investigator}}
Origin Domain       : {{investigation.origin_domain.ancestor_names_plain|safe}}
Detection Domain    : {{investigation.detection_domain.ancestor_names_plain|safe}}
Degree of Harm      : {{investigation.get_harm_display}}
Cause               : {{investigation.cause.ancestor_names_plain|safe}}

=== Unsubscribe ===

Use the following link to unsubscribe from this incident:

{{unsubscribe}}
