from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType
from django.core.mail import send_mail
from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver, Signal
from django.template import Context
from django.template.loader import get_template

import models


incident_submitted = Signal(providing_args=["incident", "user", "subscribe"])
incident_completed = Signal(providing_args=["incident"])
investigator_assigned = Signal(providing_args=["incident"])
investigator_unassigned = Signal(providing_args=["incident"])
incident_actions = Signal(providing_args=["incident", "actions"])
incident_action_assigned = Signal(providing_args=["action"])
incident_action_unassigned = Signal(providing_args=["action"])
incident_invalid = Signal(providing_args=["incident", "comment"])
incident_reopened = Signal(providing_args=["incident", "comment"])


def build_url(request, relative_path):
    if "HTTP_X_FORWARDED_HOST" not in request.META:
        return request.build_absolute_uri(relative_path)

    host = request.META["HTTP_X_FORWARDED_HOST"]
    http = 'https' if 'https' in request.META["wsgi.url_scheme"] or 'https' in request.META["HTTP_REFERER"] else 'http'
    return "%s://%s%s" % (http, host, relative_path)


def incident_url(request, incident):
    return build_url(request, incident.get_absolute_url())


def unsubscribe_url(request, incident):
    url = reverse("unsubscribe", kwargs={"pk":incident.pk})
    return build_url(request, url)


@receiver(incident_submitted)
def incident_submitted_handler(sender, **kwargs ):

    incident = kwargs["incident"]
    user = kwargs["user"]
    subscribe = kwargs["subscribe"]
    url = incident_url(sender.request, incident)
    unsub_url = unsubscribe_url(sender.request, incident)

    if subscribe and user.email:
        sub = models.Subscription(user=user, incident=incident)
        sub.save()

    emails = [incident.submitted_by.email ] if incident.submitted_by.email else None
    if not emails:
        return

    template = get_template("notifications/email/incident_submitted.txt")
    context =  Context({"url":url, "incident":incident, "subscribe":subscribe, "user":user })
    subject = "SaILS - Incident #%d Submitted" % (incident.pk)
    content = template.render(context)

    send_mail(subject, content, settings.NOTIFICATIONS_EMAIL, emails, fail_silently=settings.NOTIFICATIONS_FAIL_SILENTLY)


@receiver(incident_completed)
def incident_completed_handler(sender, **kwargs ):

    incident = kwargs["incident"]
    url = incident_url(sender.request, incident)
    unsub_url = unsubscribe_url(sender.request, incident)

    subs = models.Subscription.objects.filter(incident=incident)
    emails = subs.values_list("user__email",flat=True)

    template = get_template("notifications/email/incident_completed.txt")
    context =  Context({ "url":url, "unsubscribe":unsub_url, "incident":incident, "investigation":incident.investigation })
    subject = "SaILS - Investigation for Incident #%d Completed" % (incident.pk)
    content = template.render(context)

    send_mail(subject, content, settings.NOTIFICATIONS_EMAIL, emails, fail_silently=settings.NOTIFICATIONS_FAIL_SILENTLY)


@receiver(investigator_assigned)
def investigator_assigned_handler(sender, **kwargs ):

    incident = kwargs["incident"]

    if not (incident.investigation.investigator and incident.investigation.investigator.investigation_notifications):
        return

    url = incident_url(sender.request, incident)
    unsub_url = unsubscribe_url(sender.request, incident)

    emails = [incident.investigation.investigator.email]

    models.Subscription.objects.get_or_create(incident=incident, user=incident.investigation.investigator)

    template = get_template("notifications/email/investigation_assigned.txt")
    context = Context({ "url":url, "unsubscribe":unsub_url, "incident":incident, "investigation":incident.investigation })
    subject = "SaILS - Incident #%d Investigation Assigned" % (incident.pk)
    content = template.render(context)

    send_mail(subject, content, settings.NOTIFICATIONS_EMAIL, emails, fail_silently=settings.NOTIFICATIONS_FAIL_SILENTLY)

@receiver(investigator_unassigned)
def investigator_unassigned_handler(sender, **kwargs ):


    incident = kwargs["incident"]
    old_investigator = kwargs["old_investigator"]

    if old_investigator is None or not old_investigator.investigation_notifications:
        return

    url = incident_url(sender.request, incident)
    unsub_url = unsubscribe_url(sender.request, incident)

    emails = [old_investigator.email]

    template = get_template("notifications/email/investigation_unassigned.txt")
    context = Context({ "url":url, "unsubscribe":unsub_url, "incident":incident, "investigation":incident.investigation, 'investigator':old_investigator })
    subject = "SaILS - Incident #%d Investigation Unassigned" % (incident.pk)
    content = template.render(context)

    send_mail(subject, content, settings.NOTIFICATIONS_EMAIL, emails, fail_silently=settings.NOTIFICATIONS_FAIL_SILENTLY)


@receiver(incident_action_assigned)
def incident_action_assigned_handler(sender, **kwargs ):
    action = kwargs["action"]
    url = incident_url(sender.request, action.incident)
    unsub_url = unsubscribe_url(sender.request, action.incident)

    if action.responsible is None or not action.responsible.action_notifications:
        return

    models.Subscription.objects.get_or_create(incident=action.incident, user=action.responsible)

    emails = [action.responsible.email]
    template = get_template("notifications/email/incident_actions_assigned.txt")
    context = Context({"url":url, "unsubscribe":unsub_url, "action":action, "incident":action.incident})
    subject = "SaILS - Incident #%d Learning Action Assigned" % (action.incident.pk)
    content = template.render(context)

    send_mail(subject, content, settings.NOTIFICATIONS_EMAIL, emails, fail_silently=settings.NOTIFICATIONS_FAIL_SILENTLY)

@receiver(incident_action_unassigned)
def incident_action_unassigned_handler(sender, **kwargs ):
    action = kwargs["action"]
    old_responsible = kwargs["old_responsible"]
    url = incident_url(sender.request, action.incident)
    unsub_url = unsubscribe_url(sender.request, action.incident)

    if old_responsible is None or not old_responsible.action_notifications:
        return

    emails = [action.responsible.email]
    template = get_template("notifications/email/incident_actions_unassigned.txt")
    context = Context({"url":url, "unsubscribe":unsub_url, "not_responsible": old_responsible, "action":action, "incident":action.incident})
    subject = "SaILS - Incident #%d Learning Action Unassigned" % (action.incident.pk)
    content = template.render(context)

    send_mail(subject, content, settings.NOTIFICATIONS_EMAIL, emails, fail_silently=settings.NOTIFICATIONS_FAIL_SILENTLY)


@receiver(incident_actions)
def incident_actions_handler(sender, **kwargs ):

    incident = kwargs["incident"]
    actions = kwargs["actions"]
    complete = [a for a in actions if a.complete]
    incomplete = [a for a in actions if not a.complete]

    url = incident_url(sender.request, incident)
    unsub_url = unsubscribe_url(sender.request, incident)

    subs = models.Subscription.objects.filter(incident=incident)
    emails = [s.user.email for s in subs if s.user.action_notifications]
    if not emails:
        return

    template = get_template("notifications/email/incident_actions.txt")
    context = Context({
        "incomplete":incomplete,
        "complete":complete,
        "incident":incident,
        "investigation":incident.investigation,
        "url": url,
        "unsubscribe":unsub_url,
    })
    subject = "SaILS - Incident #%d Learning Actions Update" % (incident.pk)
    content = template.render(context)

    send_mail(subject, content, settings.NOTIFICATIONS_EMAIL, emails, fail_silently=settings.NOTIFICATIONS_FAIL_SILENTLY)


@receiver(incident_invalid)
def incident_invalid_handler(sender, **kwargs ):
    incident = kwargs["incident"]
    comment = kwargs["comment"]
    url = incident_url(sender.request, incident)
    unsub_url = unsubscribe_url(sender.request, incident)

    subs = models.Subscription.objects.filter(incident=incident)
    emails = subs.values_list("user__email",flat=True)
    template = get_template("notifications/email/incident_invalid.txt")
    context = Context({ "url":url, "unsubscribe":unsub_url, "incident":incident, "investigation":incident.investigation })
    subject = "SaILS - Incident #%d Closed" % (incident.pk)
    content = template.render(context)

    send_mail(subject, content, settings.NOTIFICATIONS_EMAIL, emails, fail_silently=settings.NOTIFICATIONS_FAIL_SILENTLY)


@receiver(incident_reopened)
def incident_reopened_handler(sender, **kwargs ):

    incident = kwargs["incident"]
    comment = kwargs["comment"]
    url = incident_url(sender.request, incident)
    unsub_url = unsubscribe_url(sender.request, incident)

    subs = models.Subscription.objects.filter(incident=incident)
    emails = subs.values_list("user__email",flat=True)
    template = get_template("notifications/email/incident_reopened.txt")
    context = Context({ "url":url, "unsubscribe":unsub_url, "incident":incident, "investigation":incident.investigation })
    subject = "SaILS - Incident #%d Reopened" % (incident.pk)
    content = template.render(context)

    send_mail(subject, content, settings.NOTIFICATIONS_EMAIL, emails, fail_silently=settings.NOTIFICATIONS_FAIL_SILENTLY)
