from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()

class Subscription(models.Model):
    user = models.ForeignKey(User)
    incident = models.ForeignKey('incidents.Incident', related_name="subscriptions")

