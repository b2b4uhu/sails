from ils import wsgi
import socket
from cherrypy import wsgiserver, config

host_name = socket.gethostname()
server_name, host = ("qatrack.ottawahospital.on.ca", "0.0.0.0") if "qatrack" in host_name else (host_name, "localhost")

if socket.gethostname() == "OHQATRACKDB01":
    sn = "qatrack.ottawahospital.on.ca"
else:
    sn = socket.gethostname()

server = wsgiserver.CherryPyWSGIServer(
    ("0.0.0.0", 8009), wsgi.application, server_name=sn,
)
server.start()
