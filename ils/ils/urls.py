from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse_lazy, reverse
from django.views.generic import TemplateView, RedirectView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url=reverse_lazy("report")),name="home"),
    url(r'^incidents/', include('incidents.urls')),
    url(r'^notifications/', include('notifications.urls')),
    url(r'^comments/', include('fluent_comments.urls')),
    url(r'^djangojs/', include('djangojs.urls')),
    url(r'^login/$', 'django.contrib.auth.views.login', name="login"),
    url(r'^logout$', 'django.contrib.auth.views.logout',{'next_page':reverse_lazy("login")}, name="logout"),
    url(r'^admin/', include(admin.site.urls)),
)
