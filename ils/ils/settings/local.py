"""Development settings and globals."""


from os.path import join, normpath

from base import *


########## DEBUG CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
TEMPLATE_DEBUG = DEBUG
########## END DEBUG CONFIGURATION


########## EMAIL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
########## END EMAIL CONFIGURATION


########## DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': normpath(join(DJANGO_ROOT, 'default.db')),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

########## END DATABASE CONFIGURATION



########## TOOLBAR CONFIGURATION
# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INSTALLED_APPS += (
        'debug_toolbar',
        'template_timings_panel',
        'template_debug',
        'devserver',
)

# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
INTERNAL_IPS = ('127.0.0.1',)


DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}


DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    "template_timings_panel.panels.TemplateTimings.TemplateTimings",
]


DEVSERVER_MODULES = (
    'devserver.modules.sql.SQLSummaryModule',
    'devserver.modules.profile.ProfileSummaryModule',
)

# See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation
MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
        #'devserver.middleware.DevServerMiddleware',
)
########## END TOOLBAR CONFIGURATION


DEFAULT_GROUPS = ["Therapy"]

AD_DNS_NAME='ohcdcgen01.civic1.ottawahospital.on.ca'

# If using non-SSL use these
AD_LDAP_PORT=389
AD_LDAP_URL='ldap://%s:%s' % (AD_DNS_NAME,AD_LDAP_PORT)

# If using SSL use these:
#AD_LDAP_PORT=636
#AD_LDAP_URL='ldaps://%s:%s' % (AD_DNS_NAME,AD_LDAP_PORT)

#AD_SEARCH_DN='dc=mygroup,dc=net,dc=com'
#AD_SEARCH_DN='OU=Users,OU=TOHCC (Cancer Centre),OU=Departments,DC=civic1,DC=ottawahospital,DC=on,DC=ca'
#AD_SEARCH_DN = "CN=Computers,DC=civic1,DC=ottawahospital,DC=on,DC=ca"
AD_SEARCH_DN = "DC=civic1,dc=ottawahospital,dc=on,dc=ca"
AD_NT4_DOMAIN='CIVIC1'
AD_SEARCH_FIELDS= ['mail','givenName','sn','sAMAccountName','memberOf']
AD_MEMBERSHIP_REQ=["*TOHCC - All Staff | Tout le personnel  - CCLHO"]
#AD_CERT_FILE='/path/to/your/cert.txt'

#AUTHENTICATION_BACKENDS += (
#    'accounts.backends.ActiveDirectoryGroupMembershipSSLBackend',
#)

AD_DEBUG=True
AD_DEBUG_FILE='./ldap.debug'
