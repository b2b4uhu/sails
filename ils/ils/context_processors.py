from django.conf import settings

def info(request):
    return {
        "VERSION":settings.VERSION,
        "BUG_REPORT_URL":settings.BUG_REPORT_URL,
        "MAX_CACHE_TIMEOUT":settings.MAX_CACHE_TIMEOUT,
    }
