$(document).ready(function(){

    $("#action-table, #incident-table").dataTable({
        iDisplayLength:5,
        sDom:'<<ir>><<rt>><<p>>'
    });

    var stack = 0,
        bars = true,
        lines = false,
        steps = false;

    var options = {
        series: {
            stack: stack,
            lines: {
                show: lines,
                fill: true,
                steps: steps
            },
            bars: {
                show: bars,
                barWidth: 0.8,
                align: "center"
            }
        },
        xaxis: {
            mode: "categories",
            tickLength: 0
        }
    };

    var count_data = _.map(type_counts,function(i,l){
        return { label:l, data:i };
    });

    var severity_data = _.map(severity_counts,function(i,l){
        return { label:l, data:i };
    });

    var status_data = _.map(status_counts,function(i,l){
        return { label:l, data:i };
    });

    $.plot("#incident-chart", count_data, options);

    $.plot("#severity-chart", severity_data, options);

    $.plot("#status-chart", status_data, options);


});
