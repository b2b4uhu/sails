"use strict";

function IncidentsViewModel(){
    var self = this;

    self.incidents = ko.observableArray([]);
    self.domains = ko.observableArray([]);
    self.investigators = ko.observableArray([]);
    self.severities = ko.observableArray([]);
    self.types = ko.observableArray([]);
    self.whos = ko.observableArray([]);
    self.actionTypes = ko.observableArray([]);

    self.active = ko.observable();
    self.incidentActions = ko.observableArray([]);

    ko.computed(function(){
        if (self.active()){
            $.getJSON(API_URL+"incidentaction?incident="+self.active().pk,function(data){

                self.incidentActions([]);
                _.each(data.results, function(action){
                    self.incidentActions.push(new IncidentAction(action));
                });
            });
        }
    });

    self.addNewAction= function(){
        self.incidentActions.push(new IncidentAction({'responsible':null, 'description':"", 'complete':false}));
    }
    self.investigatorName= function(investigation){
        var investigator = investigation.investigator();
        if (!investigator){
            return "";
        }
        var inv_obj  = _.find(self.investigators(),function(i){
            return i.url === investigator;
        });
        return '<span title="Assigned '+investigation.assigned + ' by '+investigation.assigned_by +'">'+inv_obj.username+'</span>';
    };

    self.whoObj= function(who){
        var who_obj  = _.find(self.whos(),function(w){
            return w.url === who;
        });
        return who_obj;
    };

    self.viewIncident = function(incident, event){
        self.viewAndHighlightIncident(incident,$(event.currentTarget).parents('tr'));
    };
    self.viewAndHighlightIncident = function(incident, row){
        row.addClass('highlight').siblings().removeClass('highlight');
        self.active(incident);
    }

    self.selectNextIncident = function(){
        var incs = self.incidents();
        if (incs.length>0){
            self.viewAndHighlightIncident(incs[0],$("tbody tr:first"));
        }
    };

    $.getJSON(API_URL+"incident",function(data){
        _.each(data.results,function(i){
            self.incidents.push(new Incident(i));
        });
        self.selectNextIncident();
    });

    var setup_calls = [
        ["severity",self.severities],
        ["who", self.whos]
    ];

    _.each(setup_calls,function(call){
        $.getJSON(API_URL+call[0],function(data){
            call[1](data.results);
        });

    });
    $.getJSON(API_URL+"user",function(data){
        _.each(data.results,function(u){
            u.text=u.username;
            u.id=u.url;
        });

        self.investigators(data.results);
    });
    $.getJSON(API_URL+"actiontype",function(data){
        _.each(data.results,function(at){
            at.text=at.name;
            at.id=at.url;
        });

        self.actionTypes(data.results);
    });
    $.getJSON(API_URL+"domain",function(data){
        var raw_domains = _.map(data.results,function(d){
            d.text=d.name;
            d.id=d.url;
            d.parentName = d.parent ? d.parent.name : null;
            return d;
        });
/*        self.domains(raw_domains);
        return;
*/
        var parents = _.filter(raw_domains, function(d){return !d.parent});
        _.each(parents,function(parent){
            parent.children = _.filter(raw_domains,function(d){return d.parent===parent.url});
            _.each(parent.children,function(c){c.parentName=parent.name;});
        });
        parents = [{id:null, url:null,name:'',text:'', children:[{id:null,url:null,text:'-- Select Domain --', name:'-- Select Domain --', children:[]}]}].concat(parents);
        self.domains(parents);
    });
}

var ivm = new IncidentsViewModel();

ivm.displayPopover = function (incident, event) {
        $(event.currentTarget).popover({
            content: incident.description,
            trigger: 'manual',
            placement: 'bottom',
            title: "Incident Description",
            container:"body"
        }).mouseout(function (e) {
            $(this).popover('hide');
        }).popover('show');
};


$.fn.editable.defaults.mode = 'inline';

ivm.inlineEdit = function (target, event, inputType) {
        inputType = inputType || 'textarea';
        $(event.currentTarget).editable({
            type:inputType,
            url:function(params){
                target(params.value);
            },
            toggle:'manual',
            inputclass:"input-xlarge",
            rows:10
        }).editable("show");
};


ko.bindingHandlers.select2 = {

    init: function(element, valueAccessor, allBindingsAccessor) {
        var obj = valueAccessor(),
            allBindings = allBindingsAccessor(),
            lookupKey = allBindings.lookupKey;
        $(element).select2(obj);
        if (lookupKey) {
            var value = ko.utils.unwrapObservable(allBindings.value);
            $(element).select2('data', ko.utils.arrayFirst(obj.data.results, function(item) {
                return item[lookupKey] === value;
            }));
        }

        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $(element).select2('destroy');
        });
    },
    update: function(element, valueAccessor, allBindings, vM, bindingContext) {
        $(element).trigger('change');
    }
};

ko.applyBindings(ivm);

function formatUser(u){
    return u.username;
}

function formatDomain(object, container){
    if (object.element && object.text !== '-- Select Domain --'){
        return object.element[0].parentNode.label+' &mdash; '+object.text;
    }
    return object.text;
}
$(document).ready(function(){
    $("#splitter").css({'height':1800}).split({orientation:'horizontal', limit:200, position:250});
});
