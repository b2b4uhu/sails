from django.test import TestCase
from incidents import models

from django.conf import settings
from django.contrib.auth import get_user_model
User = get_user_model()
from django.core.urlresolvers import reverse
from django.http import Http404
from django.test.client import Client

import django.utils.timezone as timezone
from faker import Faker
import random

faker = Faker()


class TestMPTT(TestCase):

    def setUp(self):

        self.c1 = models.Cause(name="c1")
        self.c1.save()
        self.c2 = models.Cause(name="c2",parent=self.c1)
        self.c2.save()
        self.c3 = models.Cause(name="c3", parent=self.c2)
        self.c3.save()

    def test_ancestor_names(self):
        self.assertEqual(u'&rarr;'.join([self.c1.name, self.c2.name, self.c3.name]), self.c3.ancestor_names())

    def test_ancestor_names_plain(self):
        self.assertEqual(u' -> '.join([self.c1.name, self.c2.name, self.c3.name]),self.c3.ancestor_names_plain())

    def test_raw_save(self):
        self.c3.lft = 99;
        self.c3.raw_save()
        self.assertEqual(self.c3.lft,99)


class TestIncidentManagers(TestCase):

    fixtures = [
        'domains_2014.json',
        #'severities.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
    ]

    def setUp(self):

        self.password = "pwd"

        self.admin1 = User.objects.create_superuser('admin1', 'myemail1@test.com', self.password)
        self.admin2 = User.objects.create_superuser('admin2', 'myemail2@test.com', self.password)

        self.valid = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            intent = models.INTENT_CHOICES[0][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[0][0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
        )

        self.valid.save()
        self.valid.save()


        self.invalid = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            intent = models.INTENT_CHOICES[0][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[0][0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
            valid=False,
        )

        self.invalid.save()

    def test_valid(self):
        self.assertListEqual(list(models.Incident.objects.valid()),[self.valid])

    def test_move_from_triage_to_incomplete(self):
        self.assertListEqual(list(models.Incident.triage.all()),[self.valid])
        self.assertListEqual(list(models.Incident.incomplete.all()),[])
        self.valid.investigation.investigator=self.admin1
        self.valid.investigation.save()
        self.assertListEqual(list(models.Incident.triage.all()),[])
        self.assertListEqual(list(models.Incident.incomplete.all()),[self.valid])


    def test_move_incomplete_to_triage(self):
        self.valid.investigation.investigator=self.admin1
        self.valid.investigation.save()
        self.assertListEqual(list(models.Incident.triage.all()),[])

        self.valid.investigation.investigator=None
        self.valid.investigation.save()
        self.assertListEqual(list(models.Incident.triage.all()),[self.valid])


    def test_complete(self):
        self.valid.investigation.complete =True
        self.valid.investigation.save()
        self.assertListEqual(list(models.Incident.complete.all()),[self.valid])




class TestIncident(TestCase):

    fixtures = [
        'domains_2014.json',
        #'severities.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
    ]

    def setUp(self):

        self.password = "pwd"

        self.admin1 = User.objects.create_superuser('admin1', 'myemail1@test.com', self.password)
        self.admin2 = User.objects.create_superuser('admin2', 'myemail2@test.com', self.password)

        self.valid = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            intent = models.INTENT_CHOICES[-1][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[-1][0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
        )

        self.valid.save()


        self.invalid = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            intent = models.INTENT_CHOICES[-1][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[-1][0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
            valid=False,
        )

        self.invalid.save()

    def test_complete_fields_set(self):
        self.assertTrue(self.valid.complete_fields_set())


    def test_complete_fields_missing_fk(self):
        self.valid.intent = None
        self.assertFalse(self.valid.complete_fields_set())

    def test_is_potential(self):
        self.assertTrue(self.valid.is_potential())
        self.assertFalse(self.valid.is_actual())

    def test_is_actual(self):
        self.valid.actual = models.ACTUAL
        self.assertTrue(self.valid.is_actual())
        self.assertFalse(self.valid.is_potential())

    def test_actual_display(self):
        self.assertEqual(self.valid.actual_display(), dict(models.ACTUAL_POTENTIAL_CHOICES)[models.POTENTIAL])

    def test_url(self):
        self.assertEqual(self.valid.get_absolute_url(),"/incidents/%d/" % self.valid.pk)


class TestActionType(TestCase):


    fixtures = [ 'actiontypes.json', ]

    def test_display(self):
        a = models.ActionType()
        self.assertEqual("",a.strength_display())
        a.strength = models.WEAKER
        self.assertEqual("Weaker",a.strength_display())

    def test_repr(self):
        a = models.ActionType(name="AType", strength = models.WEAKER)
        self.assertEqual("AType (Weaker)", a.__unicode__())


class TestInvestigation(TestCase):

    fixtures = [
        'domains_2014.json',
        #'severities.json',
        'techniques.json',
        'actionescalation.json',
        'actionpriority.json',
        'actiontypes.json',
        'causes.json',
        'locations.json',
        'standarddescriptions.json',
    ]

    def setUp(self):

        self.password = "pwd"

        self.admin1 = User.objects.create_superuser('admin1', 'myemail1@test.com', self.password)
        self.admin2 = User.objects.create_superuser('admin2', 'myemail2@test.com', self.password)

        self.valid = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            intent = models.INTENT_CHOICES[-1][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[-1][0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
        )

        self.valid.save()

        i = self.valid.investigation
        i.investigator=self.admin1
        i.cause = random.choice(models.Cause.objects.all())
        i.detection_domain = random.choice(models.Domain.objects.all())
        i.origin_domain = random.choice(models.Domain.objects.all())
        i.harm = random.choice(models.DOH_CHOICES)[0]
        i.save()


        self.invalid = models.Incident(
            incident_date=timezone.now().date(),
            incident_type=models.CLINICAL,
            severity=random.choice(models.SEVERITY_CHOICES)[0],
            intent = models.INTENT_CHOICES[-1][0],
            technique=random.choice(models.Technique.objects.all()),
            num_affected=models.NUM_AFFECTED_CHOICES[-1][0],
            location=random.choice(models.LocationChoice.objects.all()),
            description=faker.text(),
            submitted_by=self.admin1,
            actual=models.POTENTIAL,
            valid=False,
        )

        self.invalid.save()

    def test_manager_incomplete(self):
        self.assertListEqual([self.valid.investigation], list(models.Investigation.objects.incomplete()))

    def test_manager_user_all(self):
        self.valid.investigation.investigator=self.admin1
        self.valid.investigation.save()
        self.invalid.investigation.investigator=self.admin1
        self.invalid.investigation.save()
        self.assertListEqual([self.valid.investigation, self.invalid.investigation], list(models.Investigation.objects.user_all(self.admin1)))

    def test_manager_user_incomplete(self):
        self.valid.investigation.investigator=self.admin1
        self.valid.investigation.save()
        self.invalid.investigation.investigator=self.admin1
        self.invalid.investigation.save()
        self.assertListEqual([self.valid.investigation], list(models.Investigation.objects.user_incomplete(self.admin1)))

    def test_complete_fields_set(self):
        self.assertTrue(self.valid.investigation.complete_fields_set())

    def test_missing_fields_actual(self):
        self.valid.actual = models.ACTUAL
        self.assertListEqual(self.valid.investigation.missing_fields(), ["psls_id", "incident__standard_description"])

    def test_missing_fields_both(self):
        self.valid.num_affected = None
        self.valid.investigation.harm = None
        self.assertListEqual(self.valid.investigation.missing_fields(), ["harm", "incident__num_affected"])

    def test_invalid_required_complete(self):
        self.assertTrue(self.invalid.investigation.required_complete())

    def test_required_complete_potential(self):
        self.assertTrue(self.valid.investigation.required_complete())

    def test_required_not_complete_actual(self):
        self.valid.actual = models.ACTUAL
        self.assertFalse(self.valid.investigation.required_complete())

    def test_required_complete_actual(self):
        self.valid.actual = models.ACTUAL
        self.valid.investigation.psls_id = "123"
        self.valid.standard_description = random.choice(models.StandardDescription.objects.all())
        self.assertTrue(self.valid.investigation.required_complete())

