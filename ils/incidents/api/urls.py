from django.conf.urls import patterns, include, url
from rest_framework import routers
import incidents.api.views as views

router = routers.DefaultRouter()
router.register(r'user', views.UserViewSet)
router.register(r'incident', views.IncidentViewSet)
router.register(r'complete', views.CompleteViewSet)
router.register(r'triage', views.TriageViewSet)
router.register(r'investigation', views.InvestigationViewSet)
router.register(r'location', views.LocationViewSet)
router.register(r'operationaltype', views.OperationalTypeViewSet)
router.register(r'actiontype', views.ActionTypeViewSet)
router.register(r'actionpriority', views.ActionPriorityViewSet)
router.register(r'actionescalation', views.ActionEscalationViewSet)
router.register(r'incidentaction', views.IncidentActionViewSet)
router.register(r'standarddescription', views.StandardDescriptionViewSet)
router.register(r'technique', views.TechniqueViewSet)
router.register(r'domain', views.DomainViewSet)
router.register(r'cause', views.CauseViewSet)

urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include("rest_framework.urls", namespace="rest_framework")),
)

