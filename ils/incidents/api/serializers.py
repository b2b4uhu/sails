from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from rest_framework import serializers
import rest_framework
import incidents.models as models

User = get_user_model()


class RelativeHyperlinkedRelatedField(serializers.HyperlinkedRelatedField):
    def get_url(self, obj, view_name, request, format):
        # by omiting request we force all urls to be relative
        return super(RelativeHyperlinkedRelatedField, self).get_url(obj, view_name, None, format)


class RelativeHyperlinkedIdentityField(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):
        # by omiting request we fserializers.ModelSerializerorce all urls to be relative
        return super(RelativeHyperlinkedIdentityField, self).get_url(obj, view_name, None, format)


class RelativeHyperlinkedModelSerializer(serializers.HyperlinkedModelSerializer):
    _hyperlink_field_class = RelativeHyperlinkedRelatedField
    _hyperlink_identify_field_class = RelativeHyperlinkedIdentityField


class UserSerializer(RelativeHyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ("pk", "url", "username", "first_name", "last_name",)


class StandardDescriptionSerializer(RelativeHyperlinkedModelSerializer):
    class Meta:
        model = models.StandardDescription


class ActionPrioritySerializer(RelativeHyperlinkedModelSerializer):
    class Meta:
        model = models.ActionPriority


class ActionEscalationSerializer(RelativeHyperlinkedModelSerializer):
    class Meta:
        model = models.ActionEscalation


class TechniqueSerializer(RelativeHyperlinkedModelSerializer):
    class Meta:
        model = models.Technique


class DomainSerializer(RelativeHyperlinkedModelSerializer):
    class Meta:
        model = models.Domain


class DomainTreeOrderSerializer(RelativeHyperlinkedModelSerializer):
    class Meta:
        model = models.Domain


class CauseSerializer(RelativeHyperlinkedModelSerializer):
    class Meta:
        model = models.Cause


class LocationSerializer(RelativeHyperlinkedModelSerializer):
    class Meta:
        model = models.LocationChoice


class OperationalTypeSerializer(RelativeHyperlinkedModelSerializer):
    class Meta:
        model = models.OperationalType


class ActionTypeSerializer(RelativeHyperlinkedModelSerializer):
    class Meta:
        model = models.ActionType


class InvestigationSerializer(RelativeHyperlinkedModelSerializer):

    pk = serializers.Field()
    investigator = RelativeHyperlinkedRelatedField(view_name='ilsuser-detail')#UserSerializer()

    assigned_by = serializers.PrimaryKeyRelatedField()
    cause = serializers.PrimaryKeyRelatedField()

    class Meta:
        model = models.Investigation


class IncidentActionSerializer(RelativeHyperlinkedModelSerializer):

    pk = serializers.Field()

    action_type = ActionTypeSerializer()
    assigned_by = serializers.RelatedField()

    responsible = serializers.HyperlinkedRelatedField(view_name='ilsuser-detail')#UserSerializer()

    class Meta:
        model = models.IncidentAction
        fields = (
            'url',
            'pk',
            'action_type',
            'responsible',
            'assigned',
            'assigned_by',
            'complete',
            'completed',
            'description',
        )


class IncidentSerializer(RelativeHyperlinkedModelSerializer):

    pk = serializers.Field()
    incident_type = serializers.Field()
    location =  LocationSerializer()
    submitted_by = serializers.RelatedField()
    investigation = InvestigationSerializer()

    action_counts = serializers.SerializerMethodField('get_action_counts')
    details_url = serializers.SerializerMethodField('get_details_url')

    class Meta:
        model = models.Incident
        fields = (
            'url',
            'pk',
            'incident_date',
            'patient_id',
            'incident_type',
            'severity',
            'location',
            'description',
            'submitted',
            'submitted_by',
            'investigation',
            'action_counts',
            'actual',
            'valid',
            'details_url',
        )


    def get_action_counts(self, obj):
        if obj:
            actions =obj.incidentaction_set.all()
            return [len(actions.filter(complete=True)), len(actions)]

    def get_details_url(self, obj):
        if obj.investigation.complete:
            return reverse('complete-details', kwargs={'pk':obj.pk})
        return reverse('incident', kwargs={'pk':obj.pk})


